const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp(functions.config().firebase);

const express = require("express");
const cors = require("cors");

const gamesController = require("./controllers/games.controller");
const roomsController = require("./controllers/rooms.controller");
const cluesController = require("./controllers/clues.controller");
const itemsController = require("./controllers/items.controller");

const api = express();

api.use(cors({ origin: true }));
api.use(express.json());

api.get("/games", gamesController.getAllGames);
api.post("/games", gamesController.postGame);
api.patch("/games/:id", gamesController.patchGame);
api.get("/rooms/:id", roomsController.getRoom);
api.post("/rooms", roomsController.postRoom);
api.patch("/rooms/:id", roomsController.patchRoom);
api.get("/clues/:id", cluesController.getClue);
api.post("/clues", cluesController.postClue);
api.patch("/clues/:id", cluesController.patchClue);
api.get("/items/:id", itemsController.getItem);
api.post("/items", itemsController.postItem);
api.patch("/items/:id", itemsController.patchItem);

exports.escapeaby = functions.https.onRequest(api);
