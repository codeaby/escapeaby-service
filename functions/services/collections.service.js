const db = require("../database/database");
const { getDocument, getDocuments } = require("../database/get-documents");

const collectionsService = (collectionName) => {
  const gamesCollection = db.collection(collectionName);

  const getAll = async () => getDocuments(await gamesCollection.get());

  const create = async (game) => await gamesCollection.add(game);

  const get = async (id) => getDocument(await gamesCollection.doc(id).get());

  const exists = async (id) => (await gamesCollection.doc(id).get()).exists;

  const update = async (id, game) => await gamesCollection.doc(id).update(game);

  return {
    getAll,
    create,
    get,
    exists,
    update,
  };
};

module.exports = collectionsService;
