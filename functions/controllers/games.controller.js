const collectionsService = require("../services/collections.service");

const gamesService = collectionsService("games");

const getAllGames = async (req, res) => {
  const games = await gamesService.getAll();

  return res.json({ games });
};

const postGame = async (req, res) => {
  const body = req.body;
  const id = (await gamesService.create(body)).id;
  const game = await gamesService.get(id);

  return res.status(201).json(game);
};

const patchGame = async (req, res) => {
  const {
    params: { id },
    body,
  } = req;

  if (!(await gamesService.exists(id))) return res.status(404).send();

  try {
    await gamesService.update(id, body);
    return res.status(204).send();
  } catch (e) {
    return res.status(400).send({ message: "Error saving" });
  }
};

module.exports = {
  getAllGames,
  postGame,
  patchGame,
};
