const collectionsService = require("../services/collections.service");

const cluesService = collectionsService("clues");

const getClue = async (req, res) => {
  const {
    params: { id },
  } = req;

  if (!(await cluesService.exists(id))) return res.status(404).send();

  const clue = await cluesService.get(id);

  return res.json(clue);
};

const postClue = async (req, res) => {
  const body = req.body;
  const id = (await cluesService.create(body)).id;
  const clue = await cluesService.get(id);

  return res.status(201).json(clue);
};

const patchClue = async (req, res) => {
  const {
    params: { id },
    body,
  } = req;

  if (!(await cluesService.exists(id))) return res.status(404).send();

  try {
    await cluesService.update(id, body);
    return res.status(204).send();
  } catch (e) {
    return res.status(400).send({ message: "Error saving" });
  }
};

module.exports = {
  getClue,
  postClue,
  patchClue,
};
