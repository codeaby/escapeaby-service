const collectionsService = require("../services/collections.service");

const roomsService = collectionsService("rooms");

const getRoom = async (req, res) => {
  const {
    params: { id },
  } = req;

  if (!(await roomsService.exists(id))) return res.status(404).send();

  const room = await roomsService.get(id);

  return res.json(room);
};

const postRoom = async (req, res) => {
  const body = req.body;
  const id = (await roomsService.create(body)).id;
  const room = await roomsService.get(id);

  return res.status(201).json(room);
};

const patchRoom = async (req, res) => {
  const {
    params: { id },
    body,
  } = req;

  if (!(await roomsService.exists(id))) return res.status(404).send();

  try {
    await roomsService.update(id, body);
    return res.status(204).send();
  } catch (e) {
    return res.status(400).send({ message: "Error saving" });
  }
};

module.exports = {
  getRoom,
  postRoom,
  patchRoom,
};
