const collectionsService = require("../services/collections.service");

const itemsService = collectionsService("items");

const getItem = async (req, res) => {
  const {
    params: { id },
  } = req;

  if (!(await itemsService.exists(id))) return res.status(404).send();

  const item = await itemsService.get(id);

  return res.json(item);
};

const postItem = async (req, res) => {
  const body = req.body;
  const id = (await itemsService.create(body)).id;
  const item = await itemsService.get(id);

  return res.status(201).json(item);
};

const patchItem = async (req, res) => {
  const {
    params: { id },
    body,
  } = req;

  if (!(await itemsService.exists(id))) return res.status(404).send();

  try {
    await itemsService.update(id, body);
    return res.status(204).send();
  } catch (e) {
    return res.status(400).send({ message: "Error saving" });
  }
};

module.exports = {
  getItem,
  postItem,
  patchItem,
};
